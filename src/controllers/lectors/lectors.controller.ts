import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CreateLectorDto } from '../../services/lectors/dto/create-lector.dto';
import { UpdateLectorDto } from '../../services/lectors/dto/update-lector.dto';
import { LectorsControllerService } from './lectors.controller.service';

@Controller('lectors')
export class LectorsController {
  constructor(
    private readonly lectorsControllerService: LectorsControllerService,
  ) {}

  @Post()
  create(@Body() createLectorDto: CreateLectorDto) {
    return this.lectorsControllerService.create(createLectorDto);
  }

  @Get()
  findAll() {
    return this.lectorsControllerService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.lectorsControllerService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateLectorDto: UpdateLectorDto) {
    return this.lectorsControllerService.update(+id, updateLectorDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.lectorsControllerService.remove(+id);
  }
}

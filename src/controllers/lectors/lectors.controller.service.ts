import { Injectable } from '@nestjs/common';
import { LectorsService } from '../../services/lectors/lectors.service';
import { CreateLectorDto } from '../../services/lectors/dto/create-lector.dto';
import { UpdateLectorDto } from '../../services/lectors/dto/update-lector.dto';

@Injectable()
export class LectorsControllerService {
  constructor(private readonly lectorsService: LectorsService) {}

  create(createLectorDto: CreateLectorDto) {
    return this.lectorsService.create(createLectorDto);
  }

  findAll() {
    return this.lectorsService.findAll();
  }

  findOne(id: string) {
    return this.lectorsService.findOne(id);
  }

  update(id: number, updateLectorDto: UpdateLectorDto) {
    return this.lectorsService.update(id, updateLectorDto);
  }

  remove(id: number) {
    return this.lectorsService.remove(id);
  }
}

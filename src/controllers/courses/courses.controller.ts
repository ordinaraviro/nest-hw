import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  // UseGuards,
} from '@nestjs/common';
import { CreateCourseDto } from '../../services/courses/dto/create-course.dto';
import { UpdateCourseDto } from '../../services/courses/dto/update-course.dto';
import { CoursesControllerService } from './courses.controller.service';
import { AddLectorToCourseDto } from 'src/services/courses/dto/add-lector-to-course.dto copy';
// import { AuthGuard } from '../../services/auth/guards/auth.guard';

@Controller('courses')
export class CoursesController {
  constructor(
    private readonly coursesControllerService: CoursesControllerService,
  ) {}

  @Post()
  create(@Body() createCourseDto: CreateCourseDto) {
    return this.coursesControllerService.create(createCourseDto);
  }

  @Get()
  // @UseGuards(AuthGuard)
  findAll() {
    return this.coursesControllerService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.coursesControllerService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCourseDto: UpdateCourseDto) {
    return this.coursesControllerService.update(+id, updateCourseDto);
  }

  @Patch(':id/add-lector-to-course')
  addLectorToCourse(
    @Param('id') id: string,
    @Body() addLectorToCourseDto: AddLectorToCourseDto,
  ) {
    return this.coursesControllerService.addLectorToCourse(
      id,
      addLectorToCourseDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.coursesControllerService.remove(+id);
  }
}

import { Module } from '@nestjs/common';
import { MarksController } from './marks.controller';
import { MarksModule } from '../../services/marks/marks.module';
import { MarksControllerService } from './marks.controller.service';

@Module({
  imports: [MarksModule],
  controllers: [MarksController],
  providers: [MarksControllerService],
})
export class MarksControllerModule {}

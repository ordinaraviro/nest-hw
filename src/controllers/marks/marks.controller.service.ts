import { Injectable } from '@nestjs/common';
import { MarksService } from '../../services/marks/marks.service';
import { CreateMarkDto } from '../../services/marks/dto/create-mark.dto';
import { UpdateMarkDto } from '../../services/marks/dto/update-mark.dto';

@Injectable()
export class MarksControllerService {
  constructor(private readonly marksService: MarksService) {}

  create(createMarkDto: CreateMarkDto) {
    return this.marksService.create(createMarkDto);
  }

  findAll() {
    return this.marksService.findAll();
  }

  findOne(id: string) {
    return this.marksService.findOne(id);
  }

  update(id: number, updateMarkDto: UpdateMarkDto) {
    return this.marksService.update(id, updateMarkDto);
  }

  remove(id: number) {
    return this.marksService.remove(id);
  }
}

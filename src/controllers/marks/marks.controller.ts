import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { MarksControllerService } from './marks.controller.service';
import { CreateMarkDto } from '../../services/marks/dto/create-mark.dto';
import { UpdateMarkDto } from '../../services/marks/dto/update-mark.dto';

@Controller('marks')
export class MarksController {
  constructor(
    private readonly marksControllerService: MarksControllerService,
  ) {}

  @Post()
  create(@Body() createMarkDto: CreateMarkDto) {
    return this.marksControllerService.create(createMarkDto);
  }

  @Get()
  findAll() {
    return this.marksControllerService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.marksControllerService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateMarkDto: UpdateMarkDto) {
    return this.marksControllerService.update(+id, updateMarkDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.marksControllerService.remove(+id);
  }
}

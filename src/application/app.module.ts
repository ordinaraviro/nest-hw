import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppService } from './app.service';
// import { APP_FILTER } from '@nestjs/core';
// import { GlobalExceptionFilter } from './middlewares/global-exception.filter';

import { GroupsModule } from '../services/groups/groups.module';
import { CoursesModule } from '../services/courses/courses.module';
import { MarksModule } from '../services/marks/marks.module';
import { LectorsModule } from '../services/lectors/lectors.module';
import { StudentsModule } from '../services/students/students.module';
import { CoursesControllerModule } from '../controllers/courses/courses.controller.module';
import { MarksControllerModule } from '../controllers/marks/marks.controller.module';
import { LectorsControllerModule } from '../controllers/lectors/lectors.controller.module';
import { GroupsControllerModule } from '../controllers/groups/groups.controller.module';
import { StudentsControllerModule } from '../controllers/students/students.controller.module';
import { typeOrmAsyncConfig } from '../configs/database/typeorm-config';
import { ConfigModule } from '../configs/config.module';
import { AuthControllerModule } from '../controllers/auth/auth.controller.module';
import { UsersModule } from '../services/users/users.module';
import { AuthModule } from '../services/auth/auth.module';
import { ResetTokenModule } from '../services/reset-token/reset-token.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync(typeOrmAsyncConfig),
    ConfigModule,
    GroupsModule,
    CoursesModule,
    MarksModule,
    LectorsModule,
    StudentsModule,
    CoursesControllerModule,
    MarksControllerModule,
    LectorsControllerModule,
    GroupsControllerModule,
    StudentsControllerModule,
    AuthControllerModule,
    UsersModule,
    AuthModule,
    ResetTokenModule,
  ],
  providers: [
    // {
    //   provide: APP_FILTER,
    //   useClass: GlobalExceptionFilter,
    // },
    AppService,
  ],
})
export class AppModule {}

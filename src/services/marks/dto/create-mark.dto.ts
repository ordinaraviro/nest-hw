import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateMarkDto {
  @IsNotEmpty()
  @ApiProperty()
  mark: number;
  student_id: number;
  lector_id: number;
  course_id: number;
}

import { Injectable } from '@nestjs/common';
import { CreateMarkDto } from './dto/create-mark.dto';
import { UpdateMarkDto } from './dto/update-mark.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Mark } from './entities/mark.entity';
import { Repository } from 'typeorm';

@Injectable()
export class MarksService {
  constructor(
    @InjectRepository(Mark)
    private readonly marksRepository: Repository<Mark>,
  ) {}

  async create(createMarkDto: CreateMarkDto) {
    return this.marksRepository.save(createMarkDto);
  }

  findAll() {
    return this.marksRepository.find({});
  }

  findOne(id: string) {
    return this.marksRepository.findOneBy({ id });
  }

  update(id: number, updateMarkDto: UpdateMarkDto) {
    return this.marksRepository.update(id, updateMarkDto);
  }

  remove(id: number) {
    return this.marksRepository.delete(id);
  }
}

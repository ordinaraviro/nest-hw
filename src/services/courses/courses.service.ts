import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Course } from './entities/course.entity';
import { Repository } from 'typeorm';
import { AddLectorToCourseDto } from './dto/add-lector-to-course.dto copy';
import { Lector } from '../lectors/entities/lector.entity';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly courseRepository: Repository<Course>,
    @InjectRepository(Lector)
    private readonly lectorsRepository: Repository<Lector>,
  ) {}

  async create(createCourseDto: CreateCourseDto) {
    const course = await this.courseRepository.findOne({
      where: {
        name: createCourseDto.name,
      },
    });

    if (course) {
      throw new BadRequestException('Course with this name already exist');
    }

    return this.courseRepository.save(createCourseDto);
  }

  findAll() {
    return this.courseRepository.find({});
  }

  async findOne(id: string) {
    const course = await this.courseRepository.findOneBy({ id });

    if (!course) {
      throw new NotFoundException('Course with this id not found');
    }

    return course;
  }

  async update(id: number, updateCourseDto: UpdateCourseDto) {
    const courseId = String(id);
    const course = await this.courseRepository.findOne({
      where: { id: courseId },
    });

    if (!course) {
      throw new NotFoundException('Course with this id not found');
    }

    return this.courseRepository.update(id, updateCourseDto);
  }

  async addLectorToCourse(
    id: string,
    addLectorToCourseDto: AddLectorToCourseDto,
  ) {
    const course = await this.courseRepository.findOne({
      where: { id },
      relations: ['lectors'],
    });

    if (!course) {
      throw new NotFoundException('Course with this id not found');
    }

    const lectorId = String(addLectorToCourseDto.id);
    if (lectorId) {
      const lector = await this.lectorsRepository.findOne({
        where: {
          id: lectorId,
        },
      });
      if (!lector) {
        throw new NotFoundException('Lector not found');
      }

      const isLectorAlreadyAssigned = course.lectors.some(
        (assignedLector) => assignedLector.id === lector.id,
      );

      if (!isLectorAlreadyAssigned) {
        course.lectors.push(lector);
        await this.courseRepository.save(course);
        return 'Lector added successfully';
      } else {
        throw new BadRequestException(
          'Lector is already assigned to the course',
        );
      }
    } else {
      throw new BadRequestException('Invalid lector ID');
    }
  }

  async remove(id: number) {
    const courseId = String(id);
    const course = await this.courseRepository.findOne({
      where: { id: courseId },
    });

    if (!course) {
      throw new NotFoundException('Course with this id not found');
    }

    return this.courseRepository.delete(id);
  }
}

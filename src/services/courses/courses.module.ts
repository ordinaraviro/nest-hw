import { Module } from '@nestjs/common';
import { CoursesService } from './courses.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Course } from './entities/course.entity';
import { Lector } from '../lectors/entities/lector.entity';
import { LectorsService } from '../lectors/lectors.service';

@Module({
  imports: [TypeOrmModule.forFeature([Course, Lector])],
  providers: [CoursesService, LectorsService],
  exports: [CoursesService, LectorsService],
})
export class CoursesModule {}

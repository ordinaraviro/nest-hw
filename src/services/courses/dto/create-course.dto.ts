import { IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateCourseDto {
  @IsString()
  @ApiProperty({
    type: String,
    description: 'The name of the course',
    example: 'Math Q1',
  })
  name: string;

  @IsString()
  @ApiProperty({
    type: String,
    description: 'The description of the course',
    example: 'An introductory math course',
  })
  description: string;

  @IsNumber()
  @ApiProperty({
    type: Number,
    description: 'The number of hours for the course',
    example: 42,
  })
  hours: number;
}

import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';

export class AddLectorToCourseDto {
  @IsNumber()
  @ApiProperty({
    type: Number,
    description: 'The id of lector',
    example: 2,
  })
  id: number;
}

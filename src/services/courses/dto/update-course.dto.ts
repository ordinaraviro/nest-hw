import { PartialType } from '@nestjs/mapped-types';
import { CreateCourseDto } from './create-course.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNumber } from 'class-validator';
import { Optional } from '@nestjs/common';

export class UpdateCourseDto extends PartialType(CreateCourseDto) {
  @IsString()
  @Optional()
  @ApiProperty({
    type: String,
    description: 'The name of the course',
    example: 'Math Q1',
  })
  name: string;

  @IsString()
  @Optional()
  @ApiProperty({
    type: String,
    description: 'The description of the course',
    example: 'An introductory math course',
  })
  description: string;

  @IsNumber()
  @Optional()
  @ApiProperty({
    type: Number,
    description: 'The number of hours for the course',
    example: 42,
  })
  hours: number;
}

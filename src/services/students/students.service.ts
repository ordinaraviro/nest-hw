import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Student } from './entities/student.entity';

@Injectable()
export class StudentsService {
  constructor(
    @InjectRepository(Student)
    private readonly studentRepository: Repository<Student>,
  ) {}

  async create(createStudentDto: CreateStudentDto) {
    const student = await this.studentRepository.findOne({
      where: {
        email: createStudentDto.email,
      },
    });

    if (student) {
      throw new BadRequestException('Student with this email already exist');
    }

    return this.studentRepository.save(createStudentDto);
  }

  findAll() {
    return this.studentRepository.find({});
  }

  async findOne(id: string) {
    const student = await this.studentRepository.findOneBy({ id });

    if (!student) {
      throw new NotFoundException('Student with this id not found');
    }

    return student;
  }

  async update(id: number, updateStudentDto: UpdateStudentDto) {
    const student = await this.studentRepository.findOne({
      where: {
        id: String(id),
      },
    });

    if (!student) {
      throw new NotFoundException('Student with this id not found');
    }
    return this.studentRepository.update(id, updateStudentDto);
  }

  async remove(id: number) {
    const student = await this.studentRepository.findOne({
      where: {
        id: String(id),
      },
    });

    if (!student) {
      throw new NotFoundException('Student with this id not found');
    }
    return this.studentRepository.delete(id);
  }
}

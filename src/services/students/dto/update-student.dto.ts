import { PartialType } from '@nestjs/mapped-types';
import { CreateStudentDto } from './create-student.dto';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsEmail,
  IsString,
  IsNumber,
  IsNotEmpty,
  IsOptional,
} from 'class-validator';

export class UpdateStudentDto extends PartialType(CreateStudentDto) {
  @IsEmail()
  @ApiProperty({
    type: String,
    description: 'The email of student',
    example: 'ex@mail.com',
  })
  email: string;

  @IsString()
  @ApiProperty({
    type: String,
    description: 'The name of student',
    example: 'John',
  })
  name: string;

  @IsString()
  @ApiProperty({
    type: String,
    description: 'The surname of student',
    example: 'Connor',
  })
  surname: string;

  @IsNumber()
  @ApiProperty({
    type: Number,
    description: 'The age of student',
    example: 22,
  })
  age: number;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    type: String,
    description: 'The img of student',
    example: 'd/coolavatar.com',
  })
  imagePath: string;

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional({
    type: Number,
    description: 'The group id of student',
    example: 2,
  })
  groupId?: number;
}

import { IsEmail, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateLectorDto {
  @IsString()
  @ApiProperty({
    type: String,
    description: 'The name of the lector',
    example: 'Stockman',
  })
  name: string;

  @IsEmail()
  @ApiProperty({
    type: String,
    description: 'The lectors email',
    example: 'email@example.com',
  })
  email: string;

  @IsString()
  @ApiProperty({
    type: String,
    description: 'The lectors password',
    example: 'qwerty',
  })
  password: string;
}

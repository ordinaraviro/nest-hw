import { PartialType } from '@nestjs/mapped-types';
import { CreateLectorDto } from './create-lector.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsEmail } from 'class-validator';

export class UpdateLectorDto extends PartialType(CreateLectorDto) {
  @IsString()
  @ApiProperty({
    type: String,
    description: 'The name of the lector',
    example: 'Stockman',
  })
  name: string;

  @IsEmail()
  @ApiProperty({
    type: String,
    description: 'The lectors email',
    example: 'email@example.com',
  })
  email: string;

  @IsString()
  @ApiProperty({
    type: String,
    description: 'The lectors password',
    example: 'qwerty',
  })
  password: string;
}

import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateLectorDto } from './dto/create-lector.dto';
import { UpdateLectorDto } from './dto/update-lector.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Lector } from './entities/lector.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class LectorsService {
  constructor(
    @InjectRepository(Lector)
    private readonly lectorsRepository: Repository<Lector>,
  ) {}

  async create(createLectorDto: CreateLectorDto) {
    const lector = await this.lectorsRepository.findOne({
      where: {
        email: createLectorDto.email,
      },
    });

    if (lector) {
      throw new BadRequestException('Lector with this email already exist');
    }

    createLectorDto.password = await bcrypt.hash(createLectorDto.password, 10);

    return this.lectorsRepository.save(createLectorDto);
  }

  findAll() {
    return this.lectorsRepository.find({
      select: ['id', 'name', 'email', 'createdAt', 'updatedAt'],
    });
  }

  async findOne(id: string) {
    const lector = await this.lectorsRepository.findOneBy({ id });

    if (!lector) {
      throw new NotFoundException('Lector with this id not found');
    }

    return this.lectorsRepository.findOne({
      select: ['id', 'name', 'email', 'createdAt', 'updatedAt'],
      relations: {
        courses: true,
      },
      where: { id },
    });
  }

  findOneByEmail(email: string) {
    return this.lectorsRepository.findOneBy({ email });
  }

  async update(id: number, updateLectorDto: UpdateLectorDto) {
    const lectorId = String(id);
    const lector = await this.lectorsRepository.findOne({
      where: { id: lectorId },
    });

    if (!lector) {
      throw new NotFoundException('Lector with this id not found');
    }

    if (updateLectorDto.email) {
      const user = await this.findOneByEmail(updateLectorDto.email);
      if (user) {
        throw new BadRequestException('This email is busy');
      }
    }

    return this.lectorsRepository.update(id, updateLectorDto);
  }

  async remove(id: number) {
    const lectorId = String(id);
    const lector = await this.lectorsRepository.findOne({
      where: { id: lectorId },
    });

    if (!lector) {
      throw new NotFoundException('Lector with this id not found');
    }

    return this.lectorsRepository.delete(id);
  }
}
